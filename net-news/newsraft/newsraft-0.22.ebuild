EAPI=8

DESCRIPTION="A lightweight feed reader with ncurses user interface inspired by Newsboat."


SRC_URI="https://codeberg.org/newsraft/newsraft/archive/${P}.tar.gz"
LICENSE="ICS"
HOMEPAGE="https://codeberg.org/grisha/newsraft"

S="${WORKDIR}/${PN}"

KEYWORDS="~amd64"


RDEPEND="
	net-misc/curl
	dev-libs/expat
	dev-libs/gumbo
	sys-libs/ncurses
	dev-db/sqlite
	dev-libs/yajl
"

BDEPEND="
	virtual/pkgconfig
	app-text/scdoc
"

SLOT=0

src_compile(){
	emake CFLAGS="${CFLAGS}" LDFLAGS="${LDFLAGS}"
}

src_install() {
	emake PREFIX="/usr" DESTDIR="${D}" install
}
