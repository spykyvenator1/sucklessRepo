EAPI=8
DESCRIPTION="a suckless ipv4 dhcp client"

SOT=0
EGIT_REPO_URI="http://git.2f30.org/sdhcp/"

src_compile() {
	make CFLAGS=${CFLAGS}
}
